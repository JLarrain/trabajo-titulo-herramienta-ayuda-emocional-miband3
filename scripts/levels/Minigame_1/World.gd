extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var hr_ = -1


# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("Mg1")
	print("Minigame 1 : Entusiasmo")
	$Transition/Changer.In(1.6)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (Input.is_action_just_pressed("HARD_RESET")):
		get_tree().reload_current_scene()
	
	if (Input.is_action_just_pressed("HARD_QUIT")):
		SfxButtonAccept.play()
		emit_signal("quit_pressed")
		get_tree().quit()
		
	var hrref = GLOBAL.get_HRREF()
	if ( hr_ < hrref && hr_ >= 0 ):
		$PlayerNode/Player.set_points(1)
		$CanvasLayer/SunMoon.texture = load("res://assets/new/Moon.png")
#		print("intencion punto=1 :: " + str($PlayerNode/Player.points_))
	else:
		if ( hr_ >= hrref ):
			$PlayerNode/Player.set_points(2)
		$CanvasLayer/SunMoon.texture = load("res://assets/new/Sun.png")
#		print("intencion punto=2 :: " + str($PlayerNode/Player.points_))


func HRTick(hr: int) -> void:
	hr_ = hr
