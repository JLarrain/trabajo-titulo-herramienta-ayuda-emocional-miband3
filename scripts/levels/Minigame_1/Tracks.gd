extends Node2D


var sonidos: Array = [
	"res://assets/new/POL-final-sacrifice-short.wav",
	"res://assets/new/POL-future-shock-short.wav",
	"res://assets/new/POL-parallel-fields-short.wav",
	"res://assets/new/POL-savage-match-short.wav",
	"res://assets/new/POL-tunnels-short.wav",
]


func _ready():
	randomize()
	_play_sound()


func _play_sound() -> void:
	var index = randi() % sonidos.size() - 1
	$Mix.stream = load(sonidos[index])
	$Mix.play()


func _on_Mix_finished():
	_play_sound()
