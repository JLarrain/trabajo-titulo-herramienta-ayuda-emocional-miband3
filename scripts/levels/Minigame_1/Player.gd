extends KinematicBody2D


const UP = Vector2(0,-1)
const FLAP = 200
const MAXFALLSPEED = 200
const GRAVITY = 10
const BOOST = 20
const BOOSTCOST = 20

var motion = Vector2()
var Wall = preload("res://scenes/levels/Minigame_1/Wallnode.tscn")
var Wall2 = preload("res://scenes/levels/Minigame_1/Wallnode2.tscn")
var Wall3 = preload("res://scenes/levels/Minigame_1/Wallnode3.tscn")
var score = 0
var points_ = 1

func _ready():
	pass
	
func _physics_process(delta):
	
	motion.y += GRAVITY
	if motion.y > MAXFALLSPEED:
		motion.y = MAXFALLSPEED
		
	if Input.is_action_just_pressed("FLAP"):
		motion.y = -FLAP
		
	if Input.is_action_just_pressed("BOOST") && score >= BOOSTCOST:
		motion.x += BOOST
		score -= BOOSTCOST
		
	motion = move_and_slide(motion, UP)
	
	get_parent().get_parent().get_node("Camera2D").shake(1,3,2)
	if position.x < -120:
		get_parent().get_parent().get_node("Camera2D").shake(1,5,5)

	if position.x < -220:
		SfxRespawn.play()
		score -= 20
		if score <= 0:
			score = 0
		position = Vector2(0,0)
		
	get_parent().get_parent().get_node("CanvasLayer/RichTextLabel").text = str(score)
		
		
func Wall_reset(case):
	var Wall_instance
	match case:
		1:
			Wall_instance = Wall.instance()
		2:
			Wall_instance = Wall2.instance()
		3:
			Wall_instance = Wall3.instance()
	Wall_instance.position = Vector2(800, rand_range(-20, 20))
	get_parent().call_deferred("add_child", Wall_instance)
	
	
func set_points(points):
	points_ = points
			
			
func _on_Resetter_body_entered(body):
	if body.name == "Walls":
		body.queue_free()
		Wall_reset(1)
	if body.name == "Walls2":
		body.queue_free()
		Wall_reset(2)
	if body.name == "Walls3":
		body.queue_free()
		Wall_reset(3)
		
		
func _on_Detect_area_entered(area):
	if area.name == "PointArea":
		SfxPoint.play()
		score += points_

		if ( int(get_parent().get_parent().get_node("CanvasLayer/RichTextLabel").text) >= GLOBAL.get_GM1SCORE() ): # 100
			$TransitionOut/Changer.Out(1.6)
			
			













