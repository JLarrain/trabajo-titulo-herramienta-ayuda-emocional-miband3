extends Node2D

var hookVelocity = 0.5
var hookAcceleration = 0.05
var hookDeceleration = 0.05
var maxVelocity = 3.0
var bounce = 0.2

var contador = 0

var hr_ = -1

var fishable = true;
var fish = preload("res://scenes/levels/Minigame_2/Fish.tscn")



func _ready():
	add_to_group("Mg2")
	print("Minigame 2 : Relajación")
	$Transition/Changer.In(1.6)
	spawn_rand()


func _process(delta):
	
	if (Input.is_action_just_pressed("HARD_QUIT")):
		SfxButtonAccept.play()
		emit_signal("quit_pressed")
		get_tree().quit()
		
	var hrref = GLOBAL.get_HRREF()
	if ( hr_ < hrref && hr_ >= 0 ):
		bounce = 0.2
		$SunMoon.texture = load("res://assets/new/Moon.png")
#		print("intencion bounce=0.2 :: " + str(bounce))
	else:
		if ( hr_ >= hrref ):
			bounce = 0.7
		$SunMoon.texture = load("res://assets/new/Sun.png")
#			print("intencion bounce=0.7 :: " + str(bounce))
	
	if ($Clicker.pressed == true):
		if hookVelocity > -maxVelocity:
			hookVelocity -= hookAcceleration
	else:
		if hookVelocity < maxVelocity:
			hookVelocity += hookDeceleration
			
	if (Input.is_action_just_pressed("FISHING")):
		hookVelocity -= .5
		
	var target = $Hook.position.y + hookVelocity
	if (target >= 280):
		hookVelocity *= -bounce
	elif (target <= 38):
		hookVelocity = 0
		$Hook.position.y = 38
	else:
		$Hook.position.y = target
		
	# Adjust Value
	if (fishable == false):
		if (len($Hook/Area2D.get_overlapping_areas()) > 0):
			$Progress.value += 125 * delta # 125
			if ($Progress.value >= 999):
				caught_fish()
		else:
			$Progress.value -= 100 * delta # 100
			if ($Progress.value <= 0):
				lost_fish()
				
				
func HRTick(hr: int) -> void:
	hr_ = hr
				
				
func caught_fish():
	SfxFishCatched.play()
	contador += 1
	$Premio/Cuenta.text = str(contador)
	
	get_node("Fish").destroy()
	$Progress.value = 0
	fishable = true
	
	premio(true)


func lost_fish():
	SfxFishFlee.play()
	get_node("Fish").destroy()
	$Progress.value = 0
	fishable = true
	
	premio(false)
	
	
func premio(win):
	
	$Premio/Cuadro.visible = true
	
	if win:
		randomize()
		var lista = [1,2,3,4,5,6,7,8,9]
#			"BlueTang",
#			"Shrimp", 
#			"Crab", 
#			"Octopus", 
#			"AngelFish",
#			"AnglerFsh", 
#			"ClownFish", 
#			"SeaStar", 
#			"SeaHorse"

		var index = randi()
		var pescado = lista[ index % lista.size() ]
		
		match pescado:
			1:
				$Premio/Pescado.set_text("BlueTang")
				$Premio/Pescados/BlueTang.visible = true
			2:
				$Premio/Pescado.set_text("Shrimp")
				$Premio/Pescados/Shrimp.visible = true
			3:
				$Premio/Pescado.set_text("Crab")
				$Premio/Pescados/Crab.visible = true
			4:
				$Premio/Pescado.set_text("Octopus")
				$Premio/Pescados/Octopus.visible = true
			5:
				$Premio/Pescado.set_text("AngelFish")
				$Premio/Pescados/AngelFish.visible = true
			6:
				$Premio/Pescado.set_text("AnglerFish")
				$Premio/Pescados/AnglerFsh.visible = true
			7:
				$Premio/Pescado.set_text("ClownFish")
				$Premio/Pescados/ClownFish.visible = true
			8:
				$Premio/Pescado.set_text("SeaStar")
				$Premio/Pescados/SeaStar.visible = true
			9:
				$Premio/Pescado.set_text("SeaHorse")
				$Premio/Pescados/SeaHorse.visible = true
				
		$Premio/Pescado.visible = true
				
		
		
	else:
		get_node("Premio/Pescado").set_text("Kelp")
		$Premio/Pescado.visible = true
		$Premio/Pescados/Kelp.visible = true
		
	get_node("NextFish").start()
	
	

func add_fish(min_d, max_d, move_speed, move_time):
	var f = fish.instance()
	f.position = Vector2($Hook.position.x, $Hook.position.y)
	
	f.min_distance = min_d
	f.max_distance = max_d
	f.movement_speed = move_speed
	f.movement_time = move_time
	
	add_child(f)
	$Progress.value = 200
	fishable = false


func spawn_rand():
	if (fishable):
		randomize()
		var dmin = rand_range(0,100)
		randomize()
		var dmax = rand_range(dmin+30,160)
		randomize()
		var mspeed = rand_range(4,10)
		randomize()
		var mtime = rand_range(2,5)

		add_fish(dmin, dmax, mspeed, mtime)
		fishable = false


#func spawn_easy():
#	if (fishable):
#		add_fish(10, 40, 8, 3)
#		fishable = false
#
#
#func spawn_medium():
#	if (fishable):
#		add_fish(30, 80, 4, 2)
#		fishable = false
#
#
#func spawn_hard():
#	if (fishable):
#		add_fish(40, 100, 4, 1.5)
#		fishable = false
#
#
#func spawn_impossible():
#	if (fishable):
#		add_fish(60, 140, 3, 1)
#		fishable = false
#
#
#func spawn_seriously():
#	if (fishable):
#		add_fish(85, 160, 2, 1)
#		fishable = false



func _on_Clicker_button_down():
	SfxRod.play()
	hookVelocity -= .5





func _on_NextFish_timeout():
	
	if ( int($Premio/Cuenta.text) >= GLOBAL.get_GM2SCORE() ):
		$TransitionOut/Changer.Out(1.6)
	
	$Premio/Pescado.visible = false
	$Premio/Cuadro.visible = false
	$Premio/Pescados/BlueTang.visible = false
	$Premio/Pescados/Shrimp.visible = false
	$Premio/Pescados/Crab.visible = false
	$Premio/Pescados/Octopus.visible = false
	$Premio/Pescados/AngelFish.visible = false
	$Premio/Pescados/AnglerFsh.visible = false
	$Premio/Pescados/ClownFish.visible = false
	$Premio/Pescados/SeaStar.visible = false
	$Premio/Pescados/SeaHorse.visible = false
	$Premio/Pescados/Kelp.visible = false

	spawn_rand()
