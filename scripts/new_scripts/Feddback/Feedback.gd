extends Control


# Called when the node enters the scene tree for the first time.
func _ready():
	var choice = GLOBAL.get_choice()
#	var choice = 1 # testing
	print(choice)
	
	match choice:
		-1:
			print("Otra vez no me siento bien señor Stark...")
			emit_signal("quit_pressed")
			get_tree().quit()
		0:
			$TransitionMg2_In/Changer.In(1.6)
			$Background1/ColorRect.visible = false
			$Background2/ColorRect.visible = true
		1:
			$TransitionMg1_In/Changer.In(1.6)
			$Background2/ColorRect.visible = false
			$Background1/ColorRect.visible = true
			
			
func _process(delta):
	if (Input.is_action_just_pressed("HARD_QUIT")):
		SfxButtonAccept.play()
		emit_signal("quit_pressed")
		get_tree().quit()


func _on_Continuar_pressed():
	SfxButtonAccept.play()
	print( "Feedback : " + str( $Node2D/HSlider.value ) )
	
	var choice = GLOBAL.get_choice()
#	var choice = 1 # testing
	match choice:
		-1:
			pass
		0:
			ChangerMg2.Out(1.6)
		1:
			ChangerMg1.Out(1.6)


func _on_Continuar_mouse_entered():
	SfxButtonHover.play()


func _on_HSlider_mouse_entered():
	SfxButtonHover.play()


func _on_HSlider_value_changed(value):
	value = value
