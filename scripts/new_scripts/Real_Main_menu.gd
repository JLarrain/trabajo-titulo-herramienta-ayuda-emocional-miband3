extends Control


onready var fadeOut = get_node("FadeOut")
export var transition_duration = 2.00
export var transition_type = 1 # TRANS_SINE


func _ready():
	$Transition/Changer.In(1.6)
	
	GLOBAL.set_choice(0) # default
	
#	$VBoxContainer/StartButton.grab_focus()



func _on_StartButton_pressed():
	SfxButtonAccept.play()
	
	fade_out(MusicPlayer)
	
	GLOBAL.set_HRREF()
	
	match GLOBAL.get_choice():
		-1:
			print("No me siento bien señor Stark...")
			emit_signal("quit_pressed")
			get_tree().quit()
		
		0: # Relajado
#			get_tree().change_scene("res://scenes/levels/Minigame_2/Minigame_2.tscn")
			$TransitionMg2/Changer.Out(1.6)

		1: # Entusiasmado
#			get_tree().change_scene("res://scenes/levels/Minigame_1/World.tscn")
			$TransitionMg1/Changer.Out(1.6)
			

func _on_OptionsButton_pressed():
	SfxButtonAccept.play()
	pass # Replace with function body.


func _on_QuitButton_pressed():
#	print( "Real Main Menu -> Quit" )
	SfxButtonAccept.play()
	emit_signal("quit_pressed")
#	print( "Real Main Menu -> Quit 2" )
	get_tree().quit()


func _on_CreditsButton_pressed():
	SfxButtonAccept.play()
	pass # Replace with function body.


func _on_OptionButton_item_selected(index):
	SfxButtonAccept.play()
	match index:
		0:
			GLOBAL.set_choice(0) # Relajado
		1:
			GLOBAL.set_choice(1) # Entusiasmado
		


func fade_out(stream_player):
	# tween music volume down to 0
	fadeOut.interpolate_property(stream_player, "volume_db", 0, -80, transition_duration, transition_type, Tween.EASE_IN, 0)
	fadeOut.start()
	# when the tween ends, the music will be stopped

func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	object.stop()


func _on_OptionButton_pressed():
	SfxButtonAccept.play()


func _on_OptionButton_mouse_entered():
	SfxButtonHover.play()


func _on_CreditsButton_mouse_entered():
	SfxButtonHover.play()


func _on_StartButton_mouse_entered():
	SfxButtonHover.play()


func _on_OptionsButton_mouse_entered():
	SfxButtonHover.play()


func _on_QuitButton_mouse_entered():
	SfxButtonHover.play()
