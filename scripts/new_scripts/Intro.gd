extends Node2D

onready var fadeIn = get_node("FadeIn")
export var transition_duration = 3.00
export var transition_type = 1 # TRANS_SINE

func _ready():

	fade_in(MusicPlayer)
	

func _on_Timer_timeout():
	$Transition/Changer.Out(1.6)


func fade_in(stream_player):
	stream_player.set_volume_db(-25)
	stream_player.play()
	
	fadeIn.interpolate_property(stream_player, "volume_db", -25, 0, transition_duration, transition_type, Tween.EASE_IN, 0)
	fadeIn.start()
	# when the tween ends, the music will be stopped

func _on_TweenOut_tween_completed(object, key):
	# stop the music -- otherwise it continues to run at silent volume
	object.stop()
