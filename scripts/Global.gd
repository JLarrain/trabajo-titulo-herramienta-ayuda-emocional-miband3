extends Node


var CHOICE

var GM1SCORE
var GM2SCORE

#var HRMIN
#var HRMAX

var HRREF
var HRREGISTRY = []
var HRREFREGISTRY = []
#var HRCURRNORM

func _ready():
	add_to_group("ReferenceHR")
	
	CHOICE = -1 # -1
	GM1SCORE = 70 # 100
	GM2SCORE = 10 # 10
#	HRMIN = 60 # 60
#	HRMAX = 80 # 80
	
#	HRREF = (HRMIN + HRMAX) / 2
#	HRCURRNORM = 1


#func get_HRCURRNORM( currentHR ):
#	var izq = 0
#	var der = HRMAX - HRMIN
#	var steps = der
	
func HR_tick(hr: int) -> void:
	HRREGISTRY.append(hr)

func get_HRREF():
#	print("HRREF: " + str(HRREF))
	return HRREF
	
func set_HRREF():
	HRREFREGISTRY = HRREGISTRY
	var zeros = 0
	var sum = 0
	for heartRate in HRREFREGISTRY:
		sum += heartRate
		if heartRate == 0:
			zeros += 1
	HRREF = sum / (HRREFREGISTRY.size() - zeros)
	print("SET HRREF: " + str(HRREF))
	
	var i = 1
	for heartRate in HRREFREGISTRY:
		print( str(i) + ": " + str(heartRate) )

#func get_HRMAX():
#	return HRMAX

#func get_HRMIN():
#	return HRMIN

func get_GM1SCORE():
	return GM1SCORE

func get_GM2SCORE():
	return GM2SCORE

func get_choice():
	return CHOICE

func set_choice(choice):
	CHOICE = choice
